## BoardTracker
BoardTracker is a virtual tabletop meant to be used with role-playing games.

### Status
This project is currently on-hold. The reason being that I started to help out
another project: [PlanarAlly](https://github.com/Kruptein/PlanarAlly).
PlanarAlly runs directly in the web browser and was built in a client-server
fashion from the start and has a lot more going for it than this project
currently.

### Features
* [ ] Character system, independent from game-system used
* [ ] Chat
* [x] Custom images
* [ ] Custom macros & scripts
* [x] Dice roller
* [x] Fog of war
* [x] Light system
* [ ] Online multiplayer
* [x] Saving boards
* [ ] SRD:s

### Building and running
BoardTracker is built in [Godot Engine](https://github.com/godotengine/godot) 3.0.2 stable. Run Godot and simply import `project.godot` and you are good to go. Press F5 inside the editor to run the project, currently there is no binary downloads because this project is very early on in development and is not very useful right now. Creating binaries right now would be a waste of time.

### License
Copyright © 2018 Jakob Sinclair

BoardTracker is licensed under the MIT License. See LICENSE file for more information.
