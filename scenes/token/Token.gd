extends Node2D

var selected = false;
# True when the cursor is over the token
var moveable = false;
var moving = false;
var font = preload("res://fonts/UI.tres").duplicate();
var tag = "Token";
var image = null;
var light = true;
var bright = 20;
var dim = 40;
var show_name = false;
var update_img = false;

func _ready():
	add_to_group("Persist");
	$Image.texture = image;
	font.set_size(24);

func _process(delta):
	var offset = $Bright.position;
	
	if ($Image.texture != null):
		$Border.visible = false;
	else:
		$Border.visible = true;
	
	update();
	
	if (selected == true):
		$Selected.modulate = Color(1, 1, 1, 1);
	else:
		$Selected.modulate = Color(1, 1, 1, 0);
	
	if (Input.is_action_just_released("drag")):
		moveable = false;
	
	if (Input.is_action_pressed("drag") && moveable == true):
		var drag = get_parent().get_local_mouse_position();
		position = drag - offset;
		moving = true;
	elif (moving):
		position.x = floor((position.x + offset.x) / 64.0) * 64;
		position.y = floor((position.y + offset.y) / 64.0) * 64;
		moving = false;
	else:
		moving = false;
	
	if (Input.is_action_pressed("deselect")):
		selected = false;
		moveable = false;
	
	if ($"/root/Tracker".Fog && light):
		$Bright.enabled = true;
		$Dim.enabled = true;
		$Bright.texture_scale = (((bright / 5.0) * 2.0) + 1.0) / 5.0;
		$Dim.texture_scale = (((dim / 5.0) * 2.0) + 1.0) / 5.0;
	else:
		$Bright.enabled = false;
		$Dim.enabled = false;
	
	if (update_img):
		$Image.texture = image;
		update_img = false;

func _on_Selected_gui_input(ev):
	if (ev.is_pressed() && ev.is_action("drag") && moving == false):
		var cam = get_node("/root/Tracker").Cam;
		if (Input.is_action_pressed("modifier")):
			if (cam != null):
				cam.selection.push_back(self);
			selected = true;
			moveable = true;
		else:
			if (cam != null):
				for t in cam.selection:
					t.selected = false;
					t.moveable = false;
				cam.selection = [self];
			selected = true;
			moveable = true;

func _draw():
	if (show_name):
		draw_string(font, Vector2(0, 0), tag, Color(0, 0, 0, 1));

func save():
	var save_dict = {
		"scene" : get_filename(),
		"tag" : tag,
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"image" : image,
		"light" : light,
		"bright" : bright,
		"dim" : dim,
		"show_name" : show_name,
	}
	return save_dict