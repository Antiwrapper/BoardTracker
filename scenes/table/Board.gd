extends Container

func _process(delta):
	if ($"../Canvas/GUI/Tools".visible == true && !$"../Canvas/GUI/Tools/CloseOpen".is_playing()):
		anchor_left = 0.2;
		margin_left = 0;
	elif ($"../Canvas/GUI/Tools/CloseOpen".is_playing()):
		anchor_left = 0;
		margin_left = $"../Canvas/GUI/Tools".rect_size.x;
	else:
		anchor_left = 0;
		margin_left = 0;