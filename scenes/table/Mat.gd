extends Control

func _process(delta):
	if (get_node("/root/Tracker").Cam != null):
		var cam = get_node("/root/Tracker").Cam;
		var offset_x = int(cam.get_camera_position().x) % 64;
		var offset_y = int(cam.get_camera_position().y) % 64;
		$Texture.rect_position = -Vector2(offset_x + 128, offset_y + 128);