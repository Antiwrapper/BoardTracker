extends Tabs

var d4 = 0;
var d6 = 0;
var d8 = 0;
var d10 = 0;
var d12 = 0;
var d20 = 0;
var d100 = 0;
var sum = 0;

func _on_Roll_pressed():
	sum = 0;
	
	for r in d4:
		sum += randi() % 4 + 1;
	for r in d6:
		sum += randi() % 6 + 1;
	for r in d8:
		sum += randi() % 8 + 1;
	for r in d10:
		sum += randi() % 10;
	for r in d12:
		sum += randi() % 12 + 1;
	for r in d20:
		sum += randi() % 20 + 1;
	for r in d100:
		sum += randi() % 100 + 1;
	$Rolled.set_text("Rolled number: " + var2str(sum));


func _on_d4_changed(value):
	d4 = int(value);

func _on_d6_changed(value):
	d6 = int(value);

func _on_d8_changed(value):
	d8 = int(value);

func _on_d10_changed(value):
	d10 = int(value);

func _on_d12_changed(value):
	d12 = int(value);

func _on_d20_changed(value):
	d20 = int(value);

func _on_d100_changed(value):
	d100 = int(value);
