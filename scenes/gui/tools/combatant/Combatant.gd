extends HBoxContainer

var token = null;
var turn = false;
var initative = 10;

func _process(delta):
	initative = $Initiative.value;
	if (token != null):
		$Tag.readonly = true;
		$Tag.text = token.tag;
		$Icons/Icon.texture = token.image;
	else:
		$Tag.readonly = false;
	if (turn == true):
		$Icons/Active.visible = true;
	else:
		$Icons/Active.visible = false;