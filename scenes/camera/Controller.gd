extends Camera2D

var last_mouse_pos = Vector2();
var moveable = false;
var selection = [];
var grid_color = Color(0.0, 0.0, 0.0, 1.0);
var grid_thick = 2;
var draw_solid = false;

func _ready():
	get_node("/root/Tracker").set_camera();
	$Mat.rect_size = get_viewport_rect().size;
	update();

func _process(delta):
	var mouse_pos = get_local_mouse_position();
	var drag = last_mouse_pos - mouse_pos;
	
	if (draw_solid):
		_process_drawing();
	else:
		_process_camera(mouse_pos, drag);
	
	last_mouse_pos = mouse_pos;

func _process_drawing():
	var mouse_pos = get_global_mouse_position();
	var mouse_local = get_local_mouse_position();
	
	if (Input.is_action_pressed("drag") && mouse_local.x > $"../Canvas/GUI/Tools".rect_size.x):
		$"../Board/Map".set_cell(int((mouse_pos.x - $"../Board".rect_position.x) / 64.0), int(mouse_pos.y / 64.0), 0);
	elif (Input.is_action_pressed("deselect")):
		print("Remove");
		$"../Board/Map".set_cell(int((mouse_pos.x - $"../Board".rect_position.x) / 64.0), int(mouse_pos.y / 64.0), -1);

func _process_camera(mouse_pos, drag):
	if (Input.is_action_just_released("drag")):
		moveable = false;
	if (Input.is_action_just_released("deselect")):
		selection = [];
	if (Input.is_action_pressed("drag") && moveable == true):
		translate(drag);
		if (global_position.x < 0):
			global_position.x = 0;
		if (global_position.y < 0):
			global_position.y = 0;
		force_update_scroll();
		update();
	if (Input.is_action_just_pressed("scroll_up")):
		self.zoom -= Vector2(0.05, 0.05);
		$Mat.rect_size = Vector2(get_viewport_rect().size * self.zoom);
		update();
	if (Input.is_action_just_pressed("scroll_down")):
		self.zoom += Vector2(0.05, 0.05);
		$Mat.rect_size = Vector2(get_viewport_rect().size * self.zoom);
		update();

func _draw():
	var lines_x = ceil((get_viewport_rect().size.x / 64) * self.zoom.x) + 1;
	var lines_y = ceil((get_viewport_rect().size.y / 64) * self.zoom.y) + 1;
	var offset_x = int(position.x) % 64 - int($"../Board".rect_position.x) % 64;
	var offset_y = int(position.y) % 64 - int($"../Board".rect_position.y) % 64;
	
	for x in lines_x:
		draw_line(Vector2(x * 64 - offset_x, 0), Vector2(x * 64 - offset_x, get_viewport_rect().size.y * self.zoom.y), grid_color, grid_thick);
	
	for y in lines_y:
		draw_line(Vector2(0, y * 64 - offset_y), Vector2(get_viewport_rect().size.x * self.zoom.x, y * 64 - offset_y), grid_color, grid_thick);

func _on_Mat_gui_input(ev):
	var moved = false;
	if (ev.is_pressed() && ev.is_action("drag")):
		for e in selection:
			if (e.moving == true):
				moved = true;
		if (moved == false):
			moveable = true;

func _on_Board_resized():
	$Mat.rect_size = Vector2(get_viewport_rect().size * self.zoom);
	$Mat.rect_position.x = int($"../Board".rect_position.x) % 64;
	$Mat.rect_position.y = int($"../Board".rect_position.y) % 64;
	update();
