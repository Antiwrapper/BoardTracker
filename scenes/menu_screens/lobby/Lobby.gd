extends Container

var my_info = {};
var player_info = {};

func _ready():
	if ($"/root/Tracker".Host):
		$Panel/List/IP/Input.editable = false;
	else:
		$Panel/List/Players.visible = false;
	get_tree().connect("network_peer_connected", self, "_player_connected");
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected");
	get_tree().connect("connected_to_server", self, "_connected_ok");
	get_tree().connect("connection_failed", self, "_connected_fail");
	get_tree().connect("server_disconnected", self, "_server_disconnected");

func _player_connected(id):
	print("Player connected");
	

func _player_disconnected(id):
	player_info.erase(id) # Erase player from info

func _connected_ok():
	# Only called on clients, not server. Send my ID and info to all the other peers
	rpc("register_player", get_tree().get_network_unique_id(), my_info);

func _server_disconnected():
	pass # Server kicked us, show error and abort

func _connected_fail():
	print("Error: could not connect to server!");

func _on_Start_pressed():
	$"/root/Tracker".IP = $Panel/List/IP/Input.text;
	$"/root/Tracker".Port = $Panel/List/Port/Input.value;
	$"/root/Tracker".MaxPlayers = $Panel/List/Players/Input.value;
	$"/root/Tracker".multiplayer();
	$Panel.visible = false;
